#JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "Https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs" | jq ' .[] | select(.name == "build_dev") | .id' )
#curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$JOB_ID/play"

#curl --request POST --header "PRIVATE-TOKEN: 9wpGhq39zFT14T5igAmh" \
#--form "variables[][key]=IMAGE_APP"
#--form "variables[][key]=IMAGE_NGINX"

curl --request POST --header "PRIVATE-TOKEN: 9wpGhq39zFT14T5igAmh" \
       --form "variables[][key]=IMAGE_APP" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$IMAGE_APP" \
       --form "variables[][key]=IMAGE_NGINX" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$IMAGE_NGINX" \
       --form "variables[][key]=PORT" \
       --form "variables[][variable_type]=env_var" \
       --form "variables[][value]=$PORT" \
	   "https://gitlab.com/api/v4/projects/20984458/pipeline?ref=master"
ID_DEPLOY=$(curl -k --location --header "PRIVATE-TOKEN: 9wpGhq39zFT14T5igAmh" "https://gitlab.com/api/v4/projects/20984458/pipelines?per_page=1&page=1" | jq '.[] | .id')
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: 9wpGhq39zFT14T5igAmh" "https://gitlab.com/api/v4/projects/20984458/pipelines/$ID_DEPLOY/jobs" | jq '.[] | select(.name == "deploy") | .id')
curl -k --request POST --header "PRIVATE-TOKEN: 9wpGhq39zFT14T5igAmh" \
	"https://gitlab.com/api/v4/projects/20984458/jobs/$JOB_ID/play"
